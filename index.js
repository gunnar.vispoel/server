import http from 'http';
import fs from 'fs';

let status;

try {
    const fileContent = fs.readFileSync('./status', 'utf8');
    status = JSON.parse(fileContent);
}
catch (error) {
    console.error(error);
    process.exit(1);
}

const server = http.createServer((req, res) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Content-Type', 'application/json');

    if (req.method === 'OPTIONS') {
        res.writeHead(200);
        res.end();
        return;
    }

    if (req.method === 'GET') {
        if (req.url === '/red') {
            res.end(JSON.stringify({ red: status.red }));
        } else if (req.url === '/blue') {
            res.end(JSON.stringify({ blue: status.blue }));
        } else if (req.url === '/status') {
            res.end(JSON.stringify({ status }));
        } else {
            res.statusCode = 404;
            res.end(JSON.stringify({ error: 'Page not found' }));
        }
    }

    if (req.method === 'PUT') {
        if (req.url === '/red') {
            status.red++;
            res.end(JSON.stringify({ red: status.red }));
        } else if (req.url === '/blue') {
            status.blue++;
            res.end(JSON.stringify({ blue: status.blue }));
        } else {
            res.statusCode = 404;
            res.end(JSON.stringify({ error: 'Page not found' }));
        }


        fs.writeFile('./status', JSON.stringify(status), (error) => {
            if (error) console.error('Error writing status file:', error);
        });
    }
});

server.listen(3000, () => {
    console.log('Listening on http://localhost:3000');
});
